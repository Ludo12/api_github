let adrr = 'https://api.github.com/users';

function rechercher() {
    if (document.getElementById('myCarousel').className == "carousel slide d-none pointer-event") {
        document.getElementById('myCarousel').className = "carousel slide d-block pointer-event";
    }
    fetch(adrr)
        .then(response => {
            return response.json();
        })
        .then(data => {
            for (let index = 0; index < 20; index++) {
                let div = document.createElement("div");
                div.id = "div_" + index;
                div.className = "carousel-item text-danger display-3 text-center";
                document.getElementById("api-g").appendChild(div);
                document.getElementById("div_" + index).innerHTML = data[index].login;
                let img = document.createElement("img");
                img.id = "img_" + index;
                img.className = "d-block w-50 mx-auto mt-3";
                document.getElementById("div_" + index).appendChild(img);
                document.getElementById("img_" + index).src = data[index].avatar_url;

            }
        });
};